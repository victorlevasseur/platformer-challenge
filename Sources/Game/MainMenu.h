#ifndef MAINMENU_H
#define MAINMENU_H

#include "AbstractMenuState.h"

class MainMenu : public AbstractMenuState
{
public:
    MainMenu();
    virtual ~MainMenu();

protected:
    virtual void OnItemSelection(GameEngine *engine, unsigned int item);
};

#endif // MAINMENU_H
