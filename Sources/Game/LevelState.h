#ifndef LEVELSTATE_H
#define LEVELSTATE_H

#include <typeindex>

#include "GameState.h"

#include "entityx/entityx.h"

#include <SFML/Graphics/Rect.hpp>

class EntityFactory;
class StateEvent;

struct LevelSettings
{
    sf::FloatRect levelBoundaries;
};

class LevelState : public GameState, public entityx::Receiver<LevelState>
{
    public:

        enum Action
        {
            Nothing,
            ChangeState,
            PushState,
            PopState
        };

        LevelState();
        virtual ~LevelState();

        virtual void ProceedEvent(GameEngine *engine, sf::Event event);
        virtual void Update(GameEngine *engine, sf::Time timeDelta);
        virtual void Render(GameEngine *engine, sf::RenderTarget &target);

        void LoadLevel(std::string levelPath);

        virtual sf::Color GetBackgroundColor() const {return sf::Color(208, 244, 248);};

        void receive(const StateEvent &event);

    protected:

        virtual void OnPlay(GameEngine *engine);
        virtual void OnStop(GameEngine *engine);
        virtual void OnPause(GameEngine *engine);
        virtual void OnResume(GameEngine *engine);

    private:
        void CreatePlayers(std::vector<int> playerIds);

        entityx::ptr<entityx::EventManager> eventManager;
        entityx::ptr<entityx::EntityManager> entityManager;
        entityx::ptr<entityx::SystemManager> systemManager;
        entityx::ptr<EntityFactory> entityFactory;

        LevelSettings levelSettings;

        Action nextAction;
        std::type_index nextState;
};

#endif // LEVELSTATE_H
