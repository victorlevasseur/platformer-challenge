#ifndef RESOURCESHOLDER_H
#define RESOURCESHOLDER_H

#include <map>
#include <string>
#include <iostream>
#include <stdexcept>

template<typename ResourceType>
class ResourcesHolder
{
public:
    ResourcesHolder() {};
    ~ResourcesHolder() {};

    void Load(const std::string &path);
    bool IsLoaded(const std::string &path) const;

    const ResourceType& Get(const std::string &path) const;

private:
    std::map<std::string, ResourceType> resources;
};

template<typename ResourceType>
void ResourcesHolder<ResourceType>::Load(const std::string &path)
{
    ResourceType res;
    if(!res.loadFromFile(path))
    {
        throw(std::runtime_error("ResourcesHolder: Can't load \"" + path + "\""));
    }

    std::cout << "Resources: \"" << path << "\" loaded." << std::endl;
    resources[path] = res;
}

template<typename ResourceType>
bool ResourcesHolder<ResourceType>::IsLoaded(const std::string &path) const
{
    return (resources.count(path) != 0);
}

template<typename ResourceType>
const ResourceType& ResourcesHolder<ResourceType>::Get(const std::string &path) const
{
    if(!IsLoaded(path))
        throw(std::logic_error("ResourcesHolder: Can't get \"" + path + "\" without loading it."));

    return resources.at(path);
}

#endif // RESOURCESHOLDER_H
