#ifndef RESOURCES_H
#define RESOURCES_H

#include <SFML/Graphics/Font.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Audio/SoundBuffer.hpp>

#include "../Singleton.h"
#include "ResourcesHolder.h"

class Resources : public Singleton<Resources>
{
public:
    ResourcesHolder<sf::Font>& GetFontHolder() {return fonts;};
    ResourcesHolder<sf::SoundBuffer>& GetSoundHolder() {return sounds;};
    ResourcesHolder<sf::Texture>& GetTextureHolder() {return textures;};

private:
    ResourcesHolder<sf::Font> fonts;
    ResourcesHolder<sf::SoundBuffer> sounds;
    ResourcesHolder<sf::Texture> textures;
};


#endif // RESOURCES_H
