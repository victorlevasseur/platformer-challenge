#include "EntityFactory.h"

#include <iostream>

#include "components/CBox.h"
#include "components/CEnemy.h"
#include "components/CFinishLine.h"
#include "components/CFollowedByCamera.h"
#include "components/CObstacle.h"
#include "components/CLinearMovement.h"
#include "components/CMovementTrigger.h"
#include "components/CPlayer.h"
#include "components/CPlayerSpawner.h"
#include "components/CPhysic.h"
#include "components/CPolygon.h"
#include "components/CRender.h"
#include "components/CZOrder.h"
#include "components/CSoundSource.h"

EntityFactory::EntityFactory(entityx::ptr<entityx::EntityManager> entityManager) : entityManager(entityManager), loadedTemplates()
{

}

void EntityFactory::CreateEntityFromSave(entityx::Entity &entity, YAML::Node node)
{
    YAML::Node componentList = node["components"];

    for(YAML::const_iterator it = componentList.begin(); it != componentList.end(); it++)
    {
        AssignComponentFromYAML(entity, (*it), false);
    }
}

void EntityFactory::CreateEntityFromTemplate(entityx::Entity &entity, std::string templateFilePath, std::string templateName)
{
    if(loadedTemplates.count(templateName) == 0)
    {
        std::vector<YAML::Node> nodes = YAML::LoadAllFromFile(templateFilePath);
        for(auto node : nodes)
        {
            if(!node["name"])
                continue;

            loadedTemplates[node["name"].as<std::string>()] = node;
        }
    }

    if(loadedTemplates.count(templateName) == 0)
    {
        std::cout << "ERROR: EntityFactory: Can't find template \"" << templateName << "\" in \"" << templateFilePath << "\" to create " << entity.id() << std::endl;
        return;
    }

    CreateEntityFromTemplate(entity, loadedTemplates[templateName]);
}

void EntityFactory::CreateEntityFromTemplate(entityx::Entity &entity, YAML::Node node)
{
    YAML::Node componentList = node["components"];

    for(YAML::const_iterator it = componentList.begin(); it != componentList.end(); it++)
    {
        AssignComponentFromYAML(entity, (*it), true);
    }
}

void EntityFactory::AssignComponentFromYAML(entityx::Entity &entity, YAML::Node compNode, bool resetIfExists)
{
    std::string compType = compNode["component"].as<std::string>();

    if(compType == "CBox")
        AssignCBox(entity, compNode, resetIfExists);
    else if(compType == "CPolygon")
        AssignCPolygon(entity, compNode, resetIfExists);
    else if(compType == "CZOrder")
        AssignCZOrder(entity, compNode, resetIfExists);
    else if(compType == "CRender")
        AssignCRender(entity, compNode, resetIfExists);
    else if(compType == "CPhysic")
        AssignCPhysic(entity, compNode, resetIfExists);
    else if(compType == "CObstacle")
        AssignCObstacle(entity, compNode, resetIfExists);
    else if(compType == "CPlayerSpawner")
        AssignCPlayerSpawner(entity, compNode, resetIfExists);
    else if(compType == "CFinishLine")
        AssignCFinishLine(entity, compNode, resetIfExists);
    else if(compType == "CPlayer")
        AssignCPlayer(entity, compNode, resetIfExists);
    else if(compType == "CEnemy")
        AssignCEnemy(entity, compNode, resetIfExists);
    else if(compType == "CFollowedByCamera")
        AssignCFollowedByCamera(entity, compNode, resetIfExists);
    else if(compType == "CLinearMovement")
        AssignCLinearMovement(entity, compNode, resetIfExists);
    else if(compType == "CMovementTrigger")
        AssignCMovementTrigger(entity, compNode, resetIfExists);
    else if(compType == "CSoundSource")
        AssignCSoundSource(entity, compNode, resetIfExists);
    else
        std::cout << "EntityFactory: Cannot assign component of type \"" << compType << "\"" << std::endl;
}

void EntityFactory::AssignCBox(entityx::Entity &entity, YAML::Node &compNode, bool resetIfExists) //DONE
{
    entityx::ptr<CBox> comp;
    if(resetIfExists && entity.component<CBox>())
    {
        entity.remove<CBox>();
        entity.assign<CBox>();
    }
    if(!entity.component<CBox>())
    {
        entity.assign<CBox>();
    }
    comp = entity.component<CBox>();

    if(compNode["x"])
        comp->x = compNode["x"].as<float>();
    if(compNode["y"])
        comp->y = compNode["y"].as<float>();
    if(compNode["width"])
        comp->width = compNode["width"].as<float>();
    if(compNode["height"])
        comp->height = compNode["height"].as<float>();
}

void EntityFactory::AssignCPolygon(entityx::Entity &entity, YAML::Node &compNode, bool resetIfExists) //DONE
{
    entityx::ptr<CPolygon> comp;
    if(resetIfExists && entity.component<CPolygon>())
    {
        entity.remove<CPolygon>();
        entity.assign<CPolygon>();
    }
    if(!entity.component<CPolygon>())
    {
        entity.assign<CPolygon>();
    }
    comp = entity.component<CPolygon>();

    if(compNode["vertices"])
    {
        std::vector<sf::Vector2f> vertices;
        for(YAML::iterator it = compNode["vertices"].begin(); it != compNode["vertices"].end(); it++)
        {
            vertices.push_back(sf::Vector2f((*it)["x"].as<float>(), (*it)["y"].as<float>()));
        }

        comp->polygon = Polygon(vertices);
    }
}

void EntityFactory::AssignCZOrder(entityx::Entity &entity, YAML::Node &compNode, bool resetIfExists) //DONE
{
    entityx::ptr<CZOrder> comp(new CZOrder());
    if(entity.component<CZOrder>())
    {
        if(!resetIfExists)
        {
            comp->layer = entity.component<CZOrder>()->layer;
            comp->zOrder = entity.component<CZOrder>()->zOrder;
        }

        entity.remove<CZOrder>();
    }

    if(compNode["zOrder"])
        comp->zOrder = compNode["zOrder"].as<int>();
    if(compNode["layer"])
        comp->layer = compNode["layer"].as<int>();

    //Force reassignement of CZOrder to update the Z-order.
    entity.assign(comp);
}

void EntityFactory::AssignCRender(entityx::Entity &entity, YAML::Node &compNode, bool resetIfExists) //DONE
{
    entityx::ptr<CRender> comp;
    if(resetIfExists && entity.component<CRender>())
    {
        entity.remove<CRender>();
    }
    if(!entity.component<CRender>())
    {
        entity.assign<CRender>();
    }
    comp = entity.component<CRender>();

    if(compNode["offset"])
    {
        sf::Vector2f pos = comp->sprite.getPosition();

        if(compNode["offset"]["x"])
            pos.x = compNode["offset"]["x"].as<float>();
        if(compNode["offset"]["y"])
            pos.y = compNode["offset"]["y"].as<float>();

        comp->sprite.setPosition(pos);
    }

    if(compNode["texturePath"])
    {
        comp->textureLoaded = false;
        comp->texturePath = compNode["texturePath"].as<std::string>();
    }

    if(compNode["subrect"])
    {
        comp->sprite.setTextureRect(sf::IntRect(compNode["subrect"][0].as<int>(),
                                                compNode["subrect"][1].as<int>(),
                                                compNode["subrect"][2].as<int>(),
                                                compNode["subrect"][3].as<int>()));
    }

    //Animations (animated tile)
    if(compNode["animations"])
    {
        comp->animated = true;
        for(YAML::iterator it = compNode["animations"].begin(); it != compNode["animations"].end(); it++)
        {
            std::string animName("unnamedAnimation");

            if((*it)["name"])
                animName = (*it)["name"].as<std::string>();

            if((*it)["directions"])
            {
                for(YAML::iterator it2 = (*it)["directions"].begin(); it2 != (*it)["directions"].end(); it2++)
                {
                    CRender::Direction direction;
                    float animDura(1.f);
                    thor::FrameAnimation anim;
                    bool animLoop(true);

                    if((*it2)["direction"])
                        direction = static_cast<CRender::Direction>((*it2)["direction"].as<int>());
                    if((*it2)["duration"])
                        animDura = (*it2)["duration"].as<float>();
                    if((*it2)["looping"])
                        animLoop = (*it2)["looping"].as<bool>();

                    if((*it2)["frames"])
                    {
                        for(YAML::iterator it3 = (*it2)["frames"].begin(); it3 != (*it2)["frames"].end(); it3++)
                        {
                            float frameDur(1.f);
                            sf::IntRect frameRect;

                            if((*it3)["duration"])
                                frameDur = (*it3)["duration"].as<float>();

                            if((*it3)["rect"])
                            {
                                if((*it3)["rect"].size() == 4)
                                {
                                    frameRect.left = (*it3)["rect"][0].as<int>();
                                    frameRect.top = (*it3)["rect"][1].as<int>();
                                    frameRect.width = (*it3)["rect"][2].as<int>();
                                    frameRect.height = (*it3)["rect"][3].as<int>();
                                }
                            }

                            anim.addFrame(frameDur, frameRect);
                        }
                    }

                    comp->animator.addAnimation(std::make_pair(animName, direction), anim, sf::seconds(animDura));
                    comp->animationLooping[std::make_pair(animName, direction)] = animLoop;
                }
            }
        }
    }

    if(compNode["defaultAnimation"] && compNode["defaultDirection"])
    {
        comp->animator.playAnimation(std::make_pair(compNode["defaultAnimation"].as<std::string>(), static_cast<CRender::Direction>(compNode["defaultDirection"].as<int>())),
                                     comp->animationLooping[std::make_pair(compNode["defaultAnimation"].as<std::string>(), static_cast<CRender::Direction>(compNode["defaultDirection"].as<int>()))]);
    }

}

void EntityFactory::AssignCPhysic(entityx::Entity &entity, YAML::Node &compNode, bool resetIfExists) //STILL LACK SOME PARAMETERS SUPPORT
{
    entityx::ptr<CPhysic> comp;
    if(resetIfExists && entity.component<CPhysic>())
    {
        entity.remove<CPhysic>();
        entity.assign<CPhysic>();
    }
    if(!entity.component<CPhysic>())
    {
        entity.assign<CPhysic>();
    }
    comp = entity.component<CPhysic>();

    if(compNode["layer"])
        comp->layer = compNode["layer"].as<int>();
    if(compNode["speed"])
        comp->maxSpeed = compNode["speed"].as<float>();
    if(compNode["jumpingSpeed"])
        comp->maxJumpingSpeed = compNode["jumpingSpeed"].as<float>();
    if(compNode["fallingSpeed"])
        comp->maxFallingSpeed = compNode["fallingSpeed"].as<float>();
    if(compNode["acceleration"])
        comp->acceleration = compNode["acceleration"].as<float>();
    if(compNode["deceleration"])
        comp->deceleration = compNode["deceleration"].as<float>();
}

void EntityFactory::AssignCObstacle(entityx::Entity &entity, YAML::Node &compNode, bool resetIfExists) // DONE
{
    entityx::ptr<CObstacle> comp;
    if(resetIfExists && entity.component<CObstacle>())
    {
        entity.remove<CObstacle>();
        entity.assign<CObstacle>();
    }
    if(!entity.component<CObstacle>())
    {
        entity.assign<CObstacle>();
    }
    comp = entity.component<CObstacle>();

    if(compNode["layer"])
        comp->layer = compNode["layer"].as<int>();

    if(compNode["type"])
    {
        if(compNode["type"].as<std::string>() == "Jumpthru")
        {
            comp->platformType = CObstacle::Jumpthru;
        }
        else
        {
            comp->platformType = CObstacle::Platform;
        }
    }
    else if(resetIfExists)
    {
        comp->platformType = CObstacle::Platform;
    }
}

void EntityFactory::AssignCPlayerSpawner(entityx::Entity &entity, YAML::Node &compNode, bool resetIfExists)
{
    entityx::ptr<CPlayerSpawner> comp;
    if(resetIfExists && entity.component<CPlayerSpawner>())
    {
        entity.remove<CPlayerSpawner>();
        entity.assign<CPlayerSpawner>();
    }
    if(!entity.component<CPlayerSpawner>())
    {
        entity.assign<CPlayerSpawner>();
    }
    comp = entity.component<CPlayerSpawner>();
}

void EntityFactory::AssignCFinishLine(entityx::Entity &entity, YAML::Node &compNode, bool resetIfExists)
{
    entityx::ptr<CFinishLine> comp;
    if(resetIfExists && entity.component<CFinishLine>())
    {
        entity.remove<CFinishLine>();
        entity.assign<CFinishLine>();
    }
    if(!entity.component<CFinishLine>())
    {
        entity.assign<CFinishLine>();
    }
    comp = entity.component<CFinishLine>();
}

void EntityFactory::AssignCPlayer(entityx::Entity &entity, YAML::Node &compNode, bool resetIfExists)
{
    entityx::ptr<CPlayer> comp;
    if(resetIfExists && entity.component<CPlayer>())
    {
        entity.remove<CPlayer>();
        entity.assign<CPlayer>();
    }
    if(!entity.component<CPlayer>())
    {
        entity.assign<CPlayer>();
    }
    comp = entity.component<CPlayer>();

    if(compNode["jumpKey"])
        comp->jumpKey = static_cast<sf::Keyboard::Key>(compNode["jumpKey"].as<int>());
    if(compNode["leftKey"])
        comp->leftKey = static_cast<sf::Keyboard::Key>(compNode["leftKey"].as<int>());
    if(compNode["rightKey"])
        comp->rightKey = static_cast<sf::Keyboard::Key>(compNode["rightKey"].as<int>());

    if(compNode["playerNumber"])
        comp->playerNumber = compNode["playerNumber"].as<int>();
    if(compNode["playerIcon"])
        comp->playerIcon = compNode["playerIcon"].as<std::string>();
}

void EntityFactory::AssignCEnemy(entityx::Entity &entity, YAML::Node &compNode, bool resetIfExists)
{
    entityx::ptr<CEnemy> comp;
    if(resetIfExists && entity.component<CEnemy>())
    {
        entity.remove<CEnemy>();
        entity.assign<CEnemy>();
    }
    if(!entity.component<CEnemy>())
    {
        entity.assign<CEnemy>();
    }
    comp = entity.component<CEnemy>();
}

void EntityFactory::AssignCFollowedByCamera(entityx::Entity &entity, YAML::Node &compNode, bool resetIfExists)
{
    entityx::ptr<CFollowedByCamera> comp;
    if(resetIfExists && entity.component<CFollowedByCamera>())
    {
        entity.remove<CFollowedByCamera>();
        entity.assign<CFollowedByCamera>();
    }
    if(!entity.component<CFollowedByCamera>())
    {
        entity.assign<CFollowedByCamera>();
    }
    comp = entity.component<CFollowedByCamera>();
}

void EntityFactory::AssignCLinearMovement(entityx::Entity &entity, YAML::Node &compNode, bool resetIfExists)
{
    entityx::ptr<CLinearMovement> comp;
    if(resetIfExists && entity.component<CLinearMovement>())
    {
        entity.remove<CLinearMovement>();
        entity.assign<CLinearMovement>();
    }
    if(!entity.component<CLinearMovement>())
    {
        entity.assign<CLinearMovement>();
    }
    comp = entity.component<CLinearMovement>();

    if(compNode["speed"])
        comp->speed = compNode["speed"].as<float>();
    if(compNode["direction"])
        comp->direction = compNode["direction"].as<int>();
}

void EntityFactory::AssignCMovementTrigger(entityx::Entity &entity, YAML::Node &compNode, bool resetIfExists)
{
    entityx::ptr<CMovementTrigger> comp;
    if(resetIfExists && entity.component<CMovementTrigger>())
    {
        entity.remove<CMovementTrigger>();
        entity.assign<CMovementTrigger>();
    }
    if(!entity.component<CMovementTrigger>())
    {
        entity.assign<CMovementTrigger>();
    }
    comp = entity.component<CMovementTrigger>();

    if(compNode["direction"])
        comp->direction = compNode["direction"].as<int>();
}

void EntityFactory::AssignCSoundSource(entityx::Entity &entity, YAML::Node &compNode, bool resetIfExists)
{
    entityx::ptr<CSoundSource> comp;
    if(resetIfExists && entity.component<CSoundSource>())
    {
        entity.remove<CSoundSource>();
        entity.assign<CSoundSource>();
    }
    if(!entity.component<CSoundSource>())
    {
        entity.assign<CSoundSource>();
    }
    comp = entity.component<CSoundSource>();

    if(compNode["sounds"])
    {
        for(YAML::const_iterator it = compNode["sounds"].begin(); it != compNode["sounds"].end(); it++)
        {
            SoundSource soundSource;
            if(it->second["path"])
            {
                soundSource.path = it->second["path"].as<std::string>();
            }
            if(it->second["repeat"])
            {
                soundSource.repeat = it->second["repeat"].as<bool>();
            }
            if(it->second["volume"])
            {
                soundSource.volume = it->second["volume"].as<int>();
            }

            comp->listOfSounds[it->first.as<std::string>()] = soundSource;
        }
    }
}
