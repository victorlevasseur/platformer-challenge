#ifndef ABSTRACTMENU_H
#define ABSTRACTMENU_H

#include <vector>
#include <string>

#include <SFML/Audio/Sound.hpp>

#include "GameState.h"

class AbstractMenuState : public GameState
{
    public:
        AbstractMenuState(const std::string &_menuTitle, const std::vector<std::string>& _items = std::vector<std::string>(), unsigned int _currentItem = 0);
        virtual ~AbstractMenuState();

        virtual void ProceedEvent(GameEngine *engine, sf::Event event);
        virtual void Update(GameEngine *engine, sf::Time timeDelta);
        virtual void Render(GameEngine *engine, sf::RenderTarget &target);

    protected:

        virtual void OnItemSelection(GameEngine *engine, unsigned int item) = 0;

        virtual void OnPlay(GameEngine *engine);
        virtual void OnStop(GameEngine *engine);
        virtual void OnPause(GameEngine *engine);
        virtual void OnResume(GameEngine *engine);

        std::string menuTitle;
        std::vector<std::string> items;
        unsigned int currentItem;

    private:
        sf::Sound buttonSound;
        sf::Sound validSound;
};

#endif // ABSTRACTMENU_H
