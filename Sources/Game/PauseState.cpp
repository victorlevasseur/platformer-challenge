#include "PauseState.h"

#include <SFML/Graphics/Font.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Text.hpp>

#include "GameEngine.h"
#include "LevelState.h"
#include "MainMenu.h"
#include "resources/Resources.h"

PauseState::PauseState() : AbstractMenuState("Pause", std::vector<std::string>()), background()
{
    items.push_back("Resume");
    items.push_back("Restart");
    items.push_back("Exit");
}

PauseState::~PauseState()
{

}

void PauseState::ProceedEvent(GameEngine *engine, sf::Event event)
{
    AbstractMenuState::ProceedEvent(engine, event);

    if(event.type == sf::Event::KeyPressed)
    {
        if(event.key.code == sf::Keyboard::Escape)
        {
            engine->PopState();
        }
    }
}

void PauseState::Update(GameEngine *engine, sf::Time timeDelta)
{
    AbstractMenuState::Update(engine, timeDelta);
}

void PauseState::Render(GameEngine *engine, sf::RenderTarget &target)
{
    //Draw the background
    sf::Sprite backgroundSprite(background);
    backgroundSprite.setColor(sf::Color(255, 255, 255, 128));
    target.draw(backgroundSprite);

    AbstractMenuState::Render(engine, target);
}

void PauseState::OnItemSelection(GameEngine *engine, unsigned int item)
{
    if(item == 0)
    {
        engine->PopState();
    }
    else if(item == 1)
    {
        engine->ChangeState<LevelState>();
    }
    else if(item == 2)
    {
        engine->Close();
    }
}

void PauseState::OnPlay(GameEngine *engine)
{
    AbstractMenuState::OnPlay(engine);

    //Create background picture from the previous state
    background.create(1024, 768);
    background.update(*dynamic_cast<sf::RenderWindow*>(&engine->GetRenderTarget()));
}

void PauseState::OnStop(GameEngine *engine)
{

}

void PauseState::OnPause(GameEngine *engine)
{

}

void PauseState::OnResume(GameEngine *engine)
{

}
