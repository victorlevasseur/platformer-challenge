#ifndef CSOUNDSOURCE_H
#define CSOUNDSOURCE_H

#include "entityx/entityx.h"

#include <list>
#include <map>
#include <SFML/Audio/Sound.hpp>

struct SoundSource
{
    std::string path;
    bool repeat;
    int volume;
};

struct CSoundSource : public entityx::Component<CSoundSource>
{
    CSoundSource() {};

    std::map<std::string, SoundSource> listOfSounds;
    std::map<std::string, sf::Sound> playingSounds;
};

#endif // CSOUNDSOURCE_H
