#ifndef CPOLYGON_H
#define CPOLYGON_H

#include "entityx/entityx.h"

#include "../polygon.h"

/**
Component : a polygonal collision mask
 - Required by : CPhysic
 - Dependencies : CBox
*/
struct CPolygon : public entityx::Component<CPolygon>
{
    CPolygon(Polygon _polygon = Polygon()) : polygon(_polygon) {};

    Polygon polygon;
};

#endif // CPOLYGON_H
