#ifndef CPHYSIC_H
#define CPHYSIC_H

#include "entityx/entityx.h"

/**
Component : contains physical data (speed, gravity...)
 - Required by : no
 - Dependencies : CPolygon
*/
struct CPhysic : public entityx::Component<CPhysic>
{
    CPhysic(int layer = 0, float maxSpeed = 250.f, float acceleration = 1500.f, float deceleration = 1500.f, float gravity = 1000.f, float maxFallingSpeed = 700.f, float maxJumpingSpeed = 900.f) :
        layer(layer),
        maxFallingSpeed(maxFallingSpeed),
        maxJumpingSpeed(maxJumpingSpeed),
        currentSpeed(0),
        fallingSpeed(0),
        jumpingSpeed(0),
        acceleration(acceleration),
        deceleration(deceleration),
        maxSpeed(maxSpeed),
        gravity(gravity),
        canJumpAgain(false),
        groundEntity(),
        oldFloorPosX(0),
        oldFloorPosY(0)
    {};

    int layer;

    float maxFallingSpeed;
    float maxJumpingSpeed;

    float currentSpeed;
    float fallingSpeed;
    float jumpingSpeed;

    float acceleration;
    float deceleration;
    float maxSpeed;

    float gravity;
    bool canJumpAgain;

    entityx::Entity groundEntity;
    float oldFloorPosX;
    float oldFloorPosY;
};

#endif // CPHYSIC_H
