#ifndef CRENDER_H
#define CRENDER_H

#include <map>

#include "entityx/entityx.h"

#include <SFML/Graphics/Color.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Texture.hpp>

#include <Thor/Animation.hpp>

/**
Component : contains render data (sprite...)
 - Required by : no
 - Dependencies : CZOrder
*/
struct CRender : public entityx::Component<CRender>
{
    enum Direction
    {
        Right = 0,
        Down = 1,
        Left = 2,
        Up = 3
    };

    CRender(std::string texturePath = "", std::string defaultAnimation = "default", Direction defaultDirection = Right) : textureLoaded(false), texturePath(texturePath), defaultAnimation(defaultAnimation), defaultDirection(defaultDirection), sprite(), animated(false), animator() {};

    bool textureLoaded;
    std::string texturePath;

    std::string defaultAnimation;
    Direction defaultDirection;

    sf::Sprite sprite;

    bool animated;
    thor::Animator<sf::Sprite,std::pair<std::string, Direction>> animator;
    std::map<std::pair<std::string, Direction>, bool> animationLooping;
};

#endif // CRENDER_H
