#ifndef CENEMY_H
#define CENEMY_H

#include "entityx/entityx.h"

/**
Component : control an enemy (with movement triggers) and also contains the enemy damages
 - Required by : no
 - Dependencies : no
*/
struct CEnemy : public entityx::Component<CEnemy>
{
    CEnemy() : currentDirection(0), wantsToJump(false) {};

    int currentDirection;
    bool wantsToJump;
};

#endif // CENEMY_H
