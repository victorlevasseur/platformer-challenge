#ifndef CZORDER_H
#define CZORDER_H

#include "entityx/entityx.h"

/**
Component : contains the Z-Order of an object
 - Required by : CRender
 - Dependencies : no
*/
struct CZOrder : public entityx::Component<CZOrder>
{
    CZOrder(int zOrder = 0, int layer = 0) : layer(layer), zOrder(zOrder) {};

    int layer;
    int zOrder;
};

#endif // CZORDER_H
