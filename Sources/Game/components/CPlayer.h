#ifndef CPLAYER_H
#define CPLAYER_H

#include "entityx/entityx.h"

#include <SFML/System/Clock.hpp>
#include <SFML/Window/Keyboard.hpp>

/**
Component : declare the entity as a player which can be moved by keyboard keys
 - Required by : no
 - Dependencies : no
*/
struct CPlayer : public entityx::Component<CPlayer>
{
    CPlayer() : jumpKey(sf::Keyboard::Space), leftKey(sf::Keyboard::Q), rightKey(sf::Keyboard::D), playerNumber(1), playerIcon(""), health(6), invincibilityTimer(), hasFinished(false) {};

    sf::Keyboard::Key jumpKey;
    sf::Keyboard::Key leftKey;
    sf::Keyboard::Key rightKey;

    int playerNumber;
    std::string playerIcon;

    int health;
    sf::Clock invincibilityTimer;

    bool hasFinished;
};

#endif // CPLAYER_H
