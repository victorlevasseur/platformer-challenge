#ifndef PLAYERHEALTHEVENT_H
#define PLAYERHEALTHEVENT_H

#include "entityx/entityx.h"

struct PlayerHealthEvent : public entityx::Event<PlayerHealthEvent>
{
    enum PlayerHealthEventType
    {
        Nothing = 0,
        LosePV,
        LoseAllPV,
        GainPV,
        GainAllPV
    };

    PlayerHealthEvent(entityx::Entity entity, PlayerHealthEventType eventType) : entity(entity), eventType(eventType) {};

    mutable entityx::Entity entity;
    PlayerHealthEventType eventType;
};

#endif // PLAYERHEALTHEVENT_H
