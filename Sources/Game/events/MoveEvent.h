#ifndef MOVEEVENT_H
#define MOVEEVENT_H

#include "entityx/entityx.h"

struct MoveEvent : public entityx::Event<MoveEvent>
{
    enum MoveType
    {
        NoMove = 0,
        GoLeft,
        GoRight,
        GoUp,
        GoDown,
        Jump
    };

    MoveEvent(entityx::Entity entity, MoveType moveType) : entity(entity), moveType(moveType) {};

    mutable entityx::Entity entity;
    MoveType moveType;
};

#endif // MOVEEVENT_H
