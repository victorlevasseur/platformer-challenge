#include "MainMenu.h"

#include "GameEngine.h"
#include "LevelState.h"

MainMenu::MainMenu() : AbstractMenuState("Platformer Challenge")
{
    items.push_back("Start the game with the launcher.");
    items.push_back("##SEPARATOR##");
    items.push_back("Exit");
}

MainMenu::~MainMenu()
{

}

void MainMenu::OnItemSelection(GameEngine *engine, unsigned int item)
{
    if(item == 2)
    {
        engine->Close();
    }
}
