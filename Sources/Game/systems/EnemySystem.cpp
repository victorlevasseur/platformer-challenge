#include "EnemySystem.h"

#include "../components/CBox.h"
#include "../components/CPlayer.h"
#include "../components/CPolygon.h"
#include "../components/CPhysic.h"
#include "../components/CEnemy.h"
#include "../components/CMovementTrigger.h"
#include "../events/MoveEvent.h"
#include "../events/PlayerHealthEvent.h"
#include "../events/SoundEvent.h"

EnemySystem::EnemySystem()
{

}

void EnemySystem::configure(entityx::ptr<entityx::EventManager> eventManager)
{

}

void EnemySystem::update(entityx::ptr<entityx::EntityManager> es, entityx::ptr<entityx::EventManager> events, double dt)
{
    std::vector<entityx::Entity> enemiesToDestroy;

    //Detect collision between players and enemies
    for(auto player : es->entities_with_components<CPlayer, CPhysic, CBox, CPolygon>())
    {
        entityx::ptr<CBox> playerCBox = player.component<CBox>();
        entityx::ptr<CPolygon> playerCPolygon = player.component<CPolygon>();
        entityx::ptr<CPhysic> playerCPhysic = player.component<CPhysic>();
        entityx::ptr<CPlayer> playerCPlayer = player.component<CPlayer>();

        float currentFallingSpeed = playerCPhysic->fallingSpeed;
        float currentJumpingSpeed = playerCPhysic->jumpingSpeed;

        //Detect if a player collides the enemy
        for(auto entity : es->entities_with_components<CEnemy, CBox, CPolygon>())
        {
            entityx::ptr<CBox> cboxComp = entity.component<CBox>();
            entityx::ptr<CPolygon> cpolyComp = entity.component<CPolygon>();
            entityx::ptr<CEnemy> cenemyComp = entity.component<CEnemy>();

            if(PolygonCollision(playerCPolygon->polygon, cpolyComp->polygon))
            {
                if(playerCBox->y + playerCBox->height - 20 < cboxComp->y && currentFallingSpeed - currentJumpingSpeed > 0)
                {
                    enemiesToDestroy.push_back(entity);

                    events->emit<SoundEvent>(player, "hit", SoundEvent::Play);

                    //Makes the player jump again
                    playerCPhysic->canJumpAgain = true;
                    playerCPhysic->jumpingSpeed = playerCPhysic->maxJumpingSpeed/2.f;
                    playerCPhysic->fallingSpeed = 0.f;
                }
                else
                {
                    if(playerCPlayer->invincibilityTimer.getElapsedTime().asSeconds() > 2.f)
                    {
                        events->emit<SoundEvent>(player, "hurt", SoundEvent::Play);

                        events->emit<PlayerHealthEvent>(player, PlayerHealthEvent::LosePV);
                        playerCPlayer->invincibilityTimer.restart();
                    }
                }
            }
        }
    }

    while(enemiesToDestroy.size() > 0)
    {
        enemiesToDestroy[0].destroy();
        enemiesToDestroy.erase(enemiesToDestroy.begin());
    }
}
