#ifndef MOVEMENTSYSTEM_H
#define MOVEMENTSYSTEM_H

#include "entityx/entityx.h"

/**
System : manage objects movement with triggers
 - Components used : CBox, CPolygon, CLinearMovement, CMovementTrigger
*/
class MovementSystem : public entityx::System<MovementSystem>, public entityx::Receiver<MovementSystem>
{
public:
    MovementSystem();

    void configure(entityx::ptr<entityx::EventManager> eventManager);
    void update(entityx::ptr<entityx::EntityManager> es, entityx::ptr<entityx::EventManager> events, double dt);
};

#endif // MOVEMENTSYSTEM_H
