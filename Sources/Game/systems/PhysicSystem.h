#ifndef PHYSICSYSTEM_H
#define PHYSICSYSTEM_H

#include <map>
#include <vector>

#include "entityx/entityx.h"

#include "../polygon.h"
#include "../components/CObstacle.h"
#include "../events/MoveEvent.h"

#define NO_EXCEPTIONS std::vector<entityx::Entity>()

/**
System : manage objects movement with collision support
 - Components used : CBox, CPolygon, CPhysic, CObstacle
*/
class PhysicSystem : public entityx::System<PhysicSystem>, public entityx::Receiver<PhysicSystem>
{
public:
    PhysicSystem();

    void configure(entityx::ptr<entityx::EventManager> eventManager);

    void update(entityx::ptr<entityx::EntityManager> es, entityx::ptr<entityx::EventManager> events, double dt);

    void receive(const MoveEvent& moveEvent);

private:
    std::vector<entityx::Entity> GetPotentialObstacles(entityx::ptr<entityx::EntityManager> es, entityx::Entity object, float maxMoveLength, int types = CObstacle::All);
    bool IsCollidingObstacle(Polygon polygon, std::vector<entityx::Entity> potentialObstacles, std::vector<entityx::Entity> except, int onlyOfType = CObstacle::All);
    bool IsCollidingObstacle(Polygon polygon, entityx::Entity obstacle);
    std::vector<entityx::Entity> GetCollidingObstacles(Polygon polygon, std::vector<entityx::Entity> potentialObstacles, std::vector<entityx::Entity> except, int types = CObstacle::All);

    bool IsOnFloor(Polygon polygon, std::vector<entityx::Entity> potentialFloors, std::vector<entityx::Entity> except = NO_EXCEPTIONS);
    entityx::Entity GetFloor(Polygon polygon, std::vector<entityx::Entity> potentialFloors, std::vector<entityx::Entity> except = NO_EXCEPTIONS);

    void MovePolygon(Polygon &poly, float dx, float dy);
    void ResetPolygonPosition(entityx::Entity entity, Polygon &poly);

    std::map<MoveEvent::MoveType, std::vector<entityx::Entity::Id>> requestedMoves;
};

#endif // PHYSICSYSTEM_H
