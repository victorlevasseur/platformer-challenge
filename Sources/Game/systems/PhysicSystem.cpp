#include "PhysicSystem.h"

#include <iostream>
#include <SFML/Window/Keyboard.hpp>

#include "../components/CBox.h"
#include "../components/CObstacle.h"
#include "../components/CPolygon.h"
#include "../components/CPhysic.h"
#include "../components/CRender.h"
#include "../events/PhysicEvent.h"
#include "../events/SoundEvent.h"

PhysicSystem::PhysicSystem() : requestedMoves()
{

}

void PhysicSystem::configure(entityx::ptr<entityx::EventManager> eventManager)
{
    eventManager->subscribe<MoveEvent>(*this);
}

void PhysicSystem::update(entityx::ptr<entityx::EntityManager> es, entityx::ptr<entityx::EventManager> events, double dt)
{
    for(auto entity : es->entities_with_components<CPhysic, CPolygon, CBox>())
    {
        entityx::ptr<CBox> objCBox = entity.component<CBox>();
        entityx::ptr<CPolygon> objCPolygon = entity.component<CPolygon>();
        entityx::ptr<CPhysic> op = entity.component<CPhysic>();

        float oldX = objCBox->x;
        float oldY = objCBox->y;

        float requestedXMove(0.f), requestedYMove(0.f);

        //Watch received events to see the requested moves
        bool wantsToJump(std::find(requestedMoves[MoveEvent::Jump].begin(), requestedMoves[MoveEvent::Jump].end(), entity.id()) != requestedMoves[MoveEvent::Jump].end());
        bool wantsToGoLeft(std::find(requestedMoves[MoveEvent::GoLeft].begin(), requestedMoves[MoveEvent::GoLeft].end(), entity.id()) != requestedMoves[MoveEvent::GoLeft].end());
        bool wantsToGoRight(std::find(requestedMoves[MoveEvent::GoRight].begin(), requestedMoves[MoveEvent::GoRight].end(), entity.id()) != requestedMoves[MoveEvent::GoRight].end());

        Polygon polygon = objCPolygon->polygon;
        polygon.ComputeGlobalVertices();
        polygon.ComputeGlobalEdges();

        //Update moving speed according to inputs
        if(wantsToGoLeft && !wantsToGoRight)
        {
            op->currentSpeed -= op->acceleration * dt;
            if(op->currentSpeed < -op->maxSpeed)
                op->currentSpeed = -op->maxSpeed;

            requestedXMove += op->currentSpeed * dt;
        }
        else if(wantsToGoRight && !wantsToGoLeft)
        {
            op->currentSpeed += op->acceleration * dt;
            if(op->currentSpeed > op->maxSpeed)
                op->currentSpeed = +op->maxSpeed;

            requestedXMove += op->currentSpeed * dt;
        }
        else
        {
            bool isPositive = op->currentSpeed > 0.f;
            if(isPositive)
            {
                op->currentSpeed -= op->deceleration * dt;
                if(op->currentSpeed < 0)
                    op->currentSpeed = 0;
            }
            else
            {
                op->currentSpeed += op->deceleration * dt;
                if(op->currentSpeed > 0)
                    op->currentSpeed = 0;
            }

            requestedXMove += op->currentSpeed * dt;
        }
        MovePolygon(polygon, requestedXMove, 0.f);

        //Get all potential obstacle
        std::vector<entityx::Entity> potentialObstacles = GetPotentialObstacles(es, entity, std::max(op->currentSpeed * dt, op->maxFallingSpeed * dt));

        //Update position according to the floor movements
        std::vector<entityx::Entity> overlappingJumpthrus = GetCollidingObstacles(polygon, potentialObstacles, NO_EXCEPTIONS, CObstacle::Jumpthru);
        std::vector<entityx::Entity>::iterator ground = std::find(overlappingJumpthrus.begin(), overlappingJumpthrus.end(), op->groundEntity); //Test the current ground (even if it's a jumpthru)
        if(ground != overlappingJumpthrus.end())
            overlappingJumpthrus.erase(ground);

        float requestedXFloorMove(0.f), requestedYFloorMove(0.f);
        if(IsOnFloor(polygon, potentialObstacles, overlappingJumpthrus) && op->groundEntity && op->groundEntity == GetFloor(polygon, potentialObstacles, overlappingJumpthrus))
        {
            entityx::ptr<CBox> floorCPos = op->groundEntity.component<CBox>();

            requestedXMove += floorCPos->x - op->oldFloorPosX;
            requestedYMove += floorCPos->y - op->oldFloorPosY;
            requestedXFloorMove = floorCPos->x - op->oldFloorPosX;
            requestedYFloorMove = floorCPos->y - op->oldFloorPosY;
        }

        //Detect collision on the X-axis
        MovePolygon(polygon, requestedXFloorMove, requestedYFloorMove);
        bool reqXPositive = requestedXMove > 0.f;

        while(IsCollidingObstacle(polygon, potentialObstacles, NO_EXCEPTIONS, CObstacle::Platform))
        {
            //Try to move the object on Y-axis to support slopes
            MovePolygon(polygon, 0.f, -1-ceil(abs(requestedXMove)));
            if(!IsCollidingObstacle(polygon, potentialObstacles, NO_EXCEPTIONS, CObstacle::Platform))
            {
                requestedYMove += -1-ceil(abs(requestedXMove));

                //Drop the object onto the obstacle
                while(!IsCollidingObstacle(polygon, potentialObstacles, NO_EXCEPTIONS, CObstacle::Platform))
                {
                    requestedYMove++;
                    MovePolygon(polygon, 0.f, 1.f);
                }
                requestedYMove--;
                MovePolygon(polygon, 0.f, -1.f);

                break;
            }

            ResetPolygonPosition(entity, polygon);
            MovePolygon(polygon, 0.f, requestedYMove);

            //Push the object back on X-axis
            requestedXMove += (reqXPositive ? -1 : 1);
            if(reqXPositive && requestedXMove < 0.f)
                requestedXMove = 0.f;
            else if(!reqXPositive && requestedXMove > 0.f)
                requestedXMove = 0.f;

            MovePolygon(polygon, requestedXMove, 0.f);
        }

        overlappingJumpthrus = GetCollidingObstacles(polygon, potentialObstacles, NO_EXCEPTIONS, CObstacle::Jumpthru); //Test the current ground (even if it's a jumpthru)
        ground = std::find(overlappingJumpthrus.begin(), overlappingJumpthrus.end(), op->groundEntity);
        if(ground != overlappingJumpthrus.end())
            overlappingJumpthrus.erase(ground);

        //Move the object on Y-axis
        // - Falling
        if(op->fallingSpeed < op->maxFallingSpeed)
        {
            op->fallingSpeed += op->gravity * dt;
        }
        else
        {
            op->fallingSpeed = op->maxFallingSpeed;
        }
        requestedYMove += op->fallingSpeed * dt;
        float requestedFall = op->fallingSpeed * dt;

        // - Jumping
        if(wantsToJump && ((IsOnFloor(polygon, potentialObstacles) && op->jumpingSpeed <= 0.f) || op->canJumpAgain))
        {
            op->jumpingSpeed = op->maxJumpingSpeed;
            op->fallingSpeed = 0.f;
            op->canJumpAgain = false;

            events->emit<SoundEvent>(entity, "jump", SoundEvent::Play);
        }
        if(op->jumpingSpeed > 0.f)
        {
            op->jumpingSpeed -= op->gravity * dt;
        }
        else if(op->jumpingSpeed < 0.f)
        {
            op->jumpingSpeed = 0.f;
        }

        requestedYMove -= op->jumpingSpeed * dt;
        requestedFall -= op->jumpingSpeed * dt;

        //Detect collision on Y-axis
        bool reqYPositive = requestedFall > 0.f;
        MovePolygon(polygon, 0.f, requestedFall);

        //Delete Jumpthru which are under or overlapping the player
        std::vector<entityx::Entity> allCollidingObstacles = GetCollidingObstacles(polygon, potentialObstacles, overlappingJumpthrus);

        while((IsCollidingObstacle(polygon, allCollidingObstacles, NO_EXCEPTIONS) && requestedYMove >= requestedYFloorMove) ||
              (IsCollidingObstacle(polygon, allCollidingObstacles, NO_EXCEPTIONS, CObstacle::Platform) && requestedYMove < requestedYFloorMove))
        {
            op->fallingSpeed = 0.f;
            op->jumpingSpeed = 0.f;

            if(reqYPositive)
            {
                requestedFall--;
                requestedYMove--;
                MovePolygon(polygon, 0.f, -1.f);
            }
            else
            {
                requestedFall++;
                requestedYMove++;
                MovePolygon(polygon, 0.f, +1.f);
            }

            //Ignore micro movement to avoid the "shaking" effect
            if(abs(requestedFall) < 1.f)
            {
                requestedYMove -= requestedFall;
                MovePolygon(polygon, 0.f, -requestedFall);
                requestedFall = 0.f;
            }
        }

        //Update current floor
        if(IsOnFloor(polygon, potentialObstacles, overlappingJumpthrus))
        {
            op->groundEntity = GetFloor(polygon, potentialObstacles, overlappingJumpthrus);

            entityx::ptr<CBox> floorCPos = op->groundEntity.component<CBox>();
            op->oldFloorPosX = floorCPos->x;
            op->oldFloorPosY = floorCPos->y;
        }
        else
        {
            op->groundEntity = entityx::Entity();
        }

        objCBox->x += requestedXMove;
        objCBox->y += requestedYMove;

        //Send messages for animations
        if(IsOnFloor(polygon, potentialObstacles, overlappingJumpthrus))
        {
            if(abs(objCBox->x - oldX - requestedXFloorMove) > 0.1f)
            {
                events->emit<SoundEvent>(entity, "walking", SoundEvent::PlayIfNot);
                events->emit<PhysicEvent>(entity, PhysicEvent::Walking);
            }
            else
            {
                events->emit<SoundEvent>(entity, "walking", SoundEvent::Stop);
                events->emit<PhysicEvent>(entity, PhysicEvent::Idle);
            }
        }
        else
        {
            events->emit<SoundEvent>(entity, "walking", SoundEvent::Stop);
            events->emit<PhysicEvent>(entity, PhysicEvent::Jump);
        }
        if(objCBox->x - oldX > requestedXFloorMove + 0.01f)
        {
            events->emit<PhysicEvent>(entity, PhysicEvent::WatchingRight);
        }
        else if(objCBox->x - oldX < requestedXFloorMove - 0.01f)
        {
            events->emit<PhysicEvent>(entity, PhysicEvent::WatchingLeft);
        }
    }

    //Clear received events
    requestedMoves.clear();
}

void PhysicSystem::receive(const MoveEvent& moveEvent)
{
    requestedMoves[moveEvent.moveType].push_back(moveEvent.entity.id());
}

std::vector<entityx::Entity> PhysicSystem::GetPotentialObstacles(entityx::ptr<entityx::EntityManager> es, entityx::Entity object, float maxMoveLength, int types)
{
    std::vector<entityx::Entity> potentialObstacles;

    entityx::ptr<CBox> cBox = object.component<CBox>();
    entityx::ptr<CPhysic> cPhysic = object.component<CPhysic>();

    if(!cBox || !object.component<CPolygon>())
        return potentialObstacles;

    sf::FloatRect objectBoundingBox;
    objectBoundingBox.left = cBox->x - maxMoveLength;
    objectBoundingBox.top = cBox->y - maxMoveLength;
    objectBoundingBox.width = cBox->width + 2*maxMoveLength;
    objectBoundingBox.height = cBox->height + 2*maxMoveLength;

    for(auto obstacle : es->entities_with_components<CObstacle, CBox, CPolygon>())
    {
        entityx::ptr<CBox> obstacleCBox = obstacle.component<CBox>();
        entityx::ptr<CObstacle> obstacleCObs = obstacle.component<CObstacle>();
        sf::FloatRect obstacleBoundingBox(obstacleCBox->x, obstacleCBox->y, obstacleCBox->width, obstacleCBox->height);

        if((obstacle.id() != object.id()) && (cPhysic->layer == obstacleCObs->layer) && ((types & obstacleCObs->platformType) != 0) && objectBoundingBox.intersects(obstacleBoundingBox))
            potentialObstacles.push_back(obstacle);
    }

    return potentialObstacles;
}

bool PhysicSystem::IsCollidingObstacle(Polygon polygon, std::vector<entityx::Entity> potentialObstacles, std::vector<entityx::Entity> except, int onlyOfType)
{
    for(auto obstacle : potentialObstacles)
    {
        //Ignore the obstacle if it's an exception
        if(std::find(except.begin(), except.end(), obstacle) != except.end())
        {
            continue;
        }

        //Get the collision polygon
        entityx::ptr<CPolygon> obstacleCPolygon = obstacle.component<CPolygon>();
        entityx::ptr<CObstacle> obstacleCO = obstacle.component<CObstacle>();

        if(!obstacleCPolygon || !obstacleCO || ((onlyOfType & obstacleCO->platformType) == 0))
        {
            continue;
        }

        //Test if there is a collision
        if(PolygonCollision(polygon, obstacleCPolygon->polygon))
        {
            return true;
        }
    }

    return false;
}

bool PhysicSystem::IsCollidingObstacle(Polygon polygon, entityx::Entity obstacle)
{
    //Get the collision polygon
    entityx::ptr<CPolygon> obstacleCPolygon = obstacle.component<CPolygon>();

    if(!obstacleCPolygon)
    {
        return false;
    }

    //Test if there is a collision
    if(PolygonCollision(polygon, obstacleCPolygon->polygon))
    {
        return true;
    }
    else
    {
        return false;
    }
}

std::vector<entityx::Entity> PhysicSystem::GetCollidingObstacles(Polygon polygon, std::vector<entityx::Entity> potentialObstacles, std::vector<entityx::Entity> except, int types)
{
    std::vector<entityx::Entity> collidingObstacles;

    for(auto obstacle : potentialObstacles)
    {
        //Ignore the obstacle if it's an exception
        if(std::find(except.begin(), except.end(), obstacle) != except.end())
        {
            continue;
        }

        //Get the collision polygon
        entityx::ptr<CPolygon> obstacleCPolygon = obstacle.component<CPolygon>();
        entityx::ptr<CObstacle> obstacleCO = obstacle.component<CObstacle>();

        if(!obstacleCPolygon || !obstacleCO || ((types & obstacleCO->platformType) == 0))
        {
            continue;
        }

        //Test if there is a collision
        if(PolygonCollision(polygon, obstacleCPolygon->polygon))
        {
            collidingObstacles.push_back(obstacle);
        }
    }

    return collidingObstacles;
}

bool PhysicSystem::IsOnFloor(Polygon polygon, std::vector<entityx::Entity> potentialFloors, std::vector<entityx::Entity> except)
{
    MovePolygon(polygon, 0, 5.f);
    return IsCollidingObstacle(polygon, potentialFloors, except);
}

entityx::Entity PhysicSystem::GetFloor(Polygon polygon, std::vector<entityx::Entity> potentialFloors, std::vector<entityx::Entity> except)
{
    MovePolygon(polygon, 0, 5.f);
    std::vector<entityx::Entity> floors = GetCollidingObstacles(polygon, potentialFloors, except);

    if(floors.size() == 0)
        return entityx::Entity();
    else
        return floors[0];
}

void PhysicSystem::MovePolygon(Polygon &poly, float dx, float dy)
{
    poly.SetOrigin(poly.GetOrigin() + sf::Vector2f(dx, dy));
    poly.ComputeGlobalVertices();
    poly.ComputeGlobalEdges();
}

void PhysicSystem::ResetPolygonPosition(entityx::Entity entity, Polygon &poly)
{
    if(!entity.component<CBox>())
        return;

    poly.SetOrigin(sf::Vector2f(entity.component<CBox>()->x + entity.component<CBox>()->width/2.f,
                                entity.component<CBox>()->y + entity.component<CBox>()->height/2.f));

    poly.ComputeGlobalVertices();
    poly.ComputeGlobalEdges();
}
