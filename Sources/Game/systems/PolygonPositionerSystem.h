#ifndef POLYGONPOSSYSTEM_H
#define POLYGONPOSSYSTEM_H

#include "entityx/entityx.h"

/**
System : update polygons origins according to the entity's position
 - Components used : CBox, CPolygon
*/
class PolygonPositionerSystem : public entityx::System<PolygonPositionerSystem>, public entityx::Receiver<PolygonPositionerSystem>
{
public:
    PolygonPositionerSystem();

    void configure(entityx::ptr<entityx::EventManager> eventManager);

    void update(entityx::ptr<entityx::EntityManager> es, entityx::ptr<entityx::EventManager> events, double dt);

private:
};

#endif // POLYGONPOSSYSTEM_H

