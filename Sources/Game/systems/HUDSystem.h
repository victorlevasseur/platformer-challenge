#ifndef HUDSYSTEM_H
#define HUDSYSTEM_H

#include "entityx/entityx.h"

namespace sf
{
    class RenderTarget;
}

/**
System : render the HUD
 - Components used : CPlayer
*/
class HUDSystem : public entityx::System<HUDSystem>, public entityx::Receiver<HUDSystem>
{
public:
    HUDSystem(sf::RenderTarget &renderTarget);

    void configure(entityx::ptr<entityx::EventManager> eventManager);

    void update(entityx::ptr<entityx::EntityManager> es, entityx::ptr<entityx::EventManager> events, double dt);

private:
    sf::RenderTarget &target;
};

#endif // HUDSYSTEM_H
