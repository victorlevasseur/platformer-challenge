#ifndef RENDERSYSTEM_H
#define RENDERSYSTEM_H

#include <set>

#include "entityx/entityx.h"

#include <SFML/Graphics/View.hpp>

class CZOrder;
class PhysicEvent;
struct LevelSettings;
namespace sf
{
    class RenderTarget;
}

/**
System : render the objects on the scene according to their Z-order
 - Components used : CBox, CRender, CZOrder
*/
class RenderSystem : public entityx::System<RenderSystem>, public entityx::Receiver<RenderSystem>
{
public:
    RenderSystem(sf::RenderTarget &renderTarget, LevelSettings &levelSettings);

    void configure(entityx::ptr<entityx::EventManager> eventManager);

    void update(entityx::ptr<entityx::EntityManager> es, entityx::ptr<entityx::EventManager> events, double dt);

    void receive(const entityx::ComponentAddedEvent<CZOrder> &event);
    void receive(const entityx::ComponentRemovedEvent<CZOrder> &event);
    void receive(const PhysicEvent &event);

private:
    static bool CompareZOrder(entityx::Entity a, entityx::Entity b);

    sf::RenderTarget &renderTarget;
    sf::View view;

    std::multiset<entityx::Entity, bool(*)(entityx::Entity, entityx::Entity)> listOfEntities;

    LevelSettings &settings;
};

#endif // RENDERSYSTEM_H
