#include "PolygonPositionerSystem.h"

#include "../components/CBox.h"
#include "../components/CPolygon.h"

PolygonPositionerSystem::PolygonPositionerSystem()
{

}

void PolygonPositionerSystem::configure(entityx::ptr<entityx::EventManager> eventManager)
{

}

void PolygonPositionerSystem::update(entityx::ptr<entityx::EntityManager> es, entityx::ptr<entityx::EventManager> events, double dt)
{
    for(auto entity : es->entities_with_components<CBox, CPolygon>())
    {
        if(entity.component<CPolygon>()->polygon.GetOrigin() == sf::Vector2f(entity.component<CBox>()->x + entity.component<CBox>()->width/2,
                                                                             entity.component<CBox>()->y + entity.component<CBox>()->height/2))
        {
            continue;
        }

        entity.component<CPolygon>()->polygon.SetOrigin(sf::Vector2f(entity.component<CBox>()->x + entity.component<CBox>()->width/2,
                                                                     entity.component<CBox>()->y + entity.component<CBox>()->height/2));

        entity.component<CPolygon>()->polygon.ComputeGlobalVertices();
        entity.component<CPolygon>()->polygon.ComputeGlobalEdges();
    }
}
