#ifndef EDITORSCENE_H
#define EDITORSCENE_H

#include <QGraphicsScene>
#include <QMap>

#include <QGraphicsSceneDragDropEvent>
#include <QGraphicsSceneMoveEvent>
#include <QKeyEvent>

#include "template.h"
#include "entity.h"

class EditorScene : public QGraphicsScene
{
    Q_OBJECT
public:

    enum EditionMode
    {
        InsertionMode,
        SelectionMode,
        FillMode,
        LevelBoundariesMode
    };

    explicit EditorScene(QObject *parent = 0);

    //List of all templates (by name)
    QMap<QString, Template> templateList;

    //Current mode
    void SetEditionMode(EditionMode mode);
    EditionMode GetEditionMode() const;

    //Set current layer and show info about layers
    void SetCurrentLayer(int layer);
    void ShowLayerColor(bool show);
    bool DoShowLayerColor() const {return showLayerColor;}

    //Get/Set the level boundaries
    QRectF GetLevelBoundaries() const {return levelBoundaries;}
    void SetLevelBoundaries(QRectF rect) {levelBoundaries = rect; UpdateBoundaries();}

    //Set the item which will be inserted after a mouse click
    void SetItemToBeInserted(QString itemName);

    //Related to the grid
    QPointF GetGridSize() const {return gridSize;}
    QPointF GetPositionOnGrid(QPointF position);

signals:
    void LevelBoundariesUpdated(QRectF boundaries);

public slots:

protected:
    virtual void drawForeground(QPainter * painter, const QRectF & rect);

    virtual void mousePressEvent(QGraphicsSceneMouseEvent * mouseEvent);
    virtual void mouseMoveEvent(QGraphicsSceneMouseEvent * mouseEvent);
    virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent * mouseEvent);

    virtual void keyReleaseEvent(QKeyEvent * keyEvent);

private:
    void ShowLayer(int layer);
    void HideLayer(int layer);

    void UpdateBoundaries();

    void Fill(QPointF firstItemPos);
    void FillAdjacentTiles(Entity *entity);

    EditionMode editionMode;

    QPointF gridSize;

    QString tileToInsert;
    QGraphicsPixmapItem *tileToInsertItem;
    QPointF lastInsertedTilePos;

    int fillIteration;

    int currentLayer;
    bool showLayerColor;

    QRectF levelBoundaries;
    QGraphicsRectItem *boundariesRectItem;
};

#endif // EDITORSCENE_H
