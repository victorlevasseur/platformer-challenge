#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <vector>
#include <QDir>
#include <QDebug>
#include <QFileDialog>
#include <QInputDialog>
#include <QMessageBox>
#include <QProcess>
#include <ostream>
#include <fstream>
#include "yaml-cpp/yaml.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    scene(new EditorScene(this)),
    currentFileName("")
{
    ui->setupUi(this);
    ui->editorView->setScene(scene);


    //Connect signals to slots
    connect(scene, SIGNAL(selectionChanged()), this, SLOT(UpdatePropertiesGrid()));
    connect(scene, SIGNAL(changed(QList<QRectF>)), this, SLOT(UpdatePropertiesGrid()));
    connect(scene, SIGNAL(LevelBoundariesUpdated(QRectF)), this, SLOT(UpdateLevelBoundaries(QRectF)));

    //Connect mode buttons to a group
    modeActionsGroup = new QActionGroup(this);
    modeActionsGroup->addAction(ui->actionInsertionMode);
    modeActionsGroup->addAction(ui->actionSelectionMode);
    modeActionsGroup->addAction(ui->actionFillMode);
    ui->actionSelectionMode->setChecked(true);
    connect(modeActionsGroup, SIGNAL(triggered(QAction*)), this, SLOT(OnModeChanged(QAction*)));
    OnModeChanged(ui->actionSelectionMode);

    //Update
    SetCurrentLayer(0);
    UpdateEntitiesList();
    UpdatePropertiesGrid();
    UpdateLevelBoundaries(scene->GetLevelBoundaries());
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::UpdateEntitiesList()
{
    ui->listWidget->clear();

    qDebug() << "Update entities list";

    QDir templateDir("./templates");

    QStringList files = templateDir.entryList(QStringList("*.entity"), QDir::Files);
    for(int a = 0; a < files.size(); a++)
    {
        std::vector<YAML::Node> templates = YAML::LoadAllFromFile("templates/" + files.at(a).toStdString());

        for(unsigned int b = 0; b < templates.size(); b++)
        {
            Template newTemplate(templates.at(b), "templates/" + files.at(a));
            scene->templateList.insert(newTemplate.name, newTemplate);

            //Add categories into the TreeWidget
            QStringList categories = newTemplate.category.split("/");
            QTreeWidgetItem *categoryItem;
            if(ui->listWidget->findItems(categories[0], Qt::MatchExactly).size() == 0)
            {
                categoryItem = new QTreeWidgetItem(QStringList(categories[0]));
                ui->listWidget->addTopLevelItem(categoryItem);
            }
            else
            {
                categoryItem = ui->listWidget->findItems(categories[0], Qt::MatchExactly)[0];
            }
            if(categories.size() > 1)
            {
                for(int a = 1; a < categories.size(); a++)
                {
                    QTreeWidgetItem *parentCategoryItem = categoryItem;
                    categoryItem = 0;
                    for(int b = 0; b < parentCategoryItem->childCount(); b++)
                    {
                        if(parentCategoryItem->child(b)->text(0) == categories[a])
                        {
                            categoryItem = parentCategoryItem->child(b);
                        }
                    }

                    if(!categoryItem)
                    {
                        categoryItem = new QTreeWidgetItem(QStringList(categories[a]));
                        parentCategoryItem->addChild(categoryItem);
                    }
                }
            }

            //Add the item
            QTreeWidgetItem *newItem = new QTreeWidgetItem(QStringList(newTemplate.fullName));
            newItem->setIcon(0, QIcon(newTemplate.iconPath));
            newItem->setFlags(Qt::ItemIsDragEnabled|Qt::ItemIsDropEnabled|newItem->flags());
            newItem->setData(0, Qt::UserRole, newTemplate.name);
            newItem->setToolTip(0, "templates/" + files.at(a) + ":" + newTemplate.name);

            categoryItem->addChild(newItem);

            qDebug() << "Added item: " << newTemplate.name;
        }
    }

    qDebug() << "... created.";
}

void MainWindow::NewLevel()
{
    currentFileName = "";

    QList<QGraphicsItem*> itemsToDelete;

    for(int a = 0; a < scene->items().size(); a++)
    {
        if(scene->items().at(a)->type() != QGraphicsItem::UserType + 1)
            continue;

        itemsToDelete.push_back(scene->items().at(a));
    }

    for(int a = 0; a < itemsToDelete.size(); a++)
    {
        scene->removeItem(itemsToDelete[a]);
    }

    scene->SetLevelBoundaries(QRectF(0, 0, 1024, 768));
}

void MainWindow::LoadLevel(QString fileName)
{
    NewLevel();

    YAML::Node file = YAML::LoadFile(fileName.toStdString());

    if(file["levelBoundaries"])
    {
        QRectF boundaries;

        boundaries.setCoords(file["levelBoundaries"]["x"].as<float>(),
                             file["levelBoundaries"]["y"].as<float>(),
                             file["levelBoundaries"]["x2"].as<float>(),
                             file["levelBoundaries"]["y2"].as<float>());

        scene->SetLevelBoundaries(boundaries);
    }

    if(!file["entities"])
        qDebug() << "Can't find the list of entities in " << fileName;

    for(YAML::iterator it = file["entities"].begin(); it != file["entities"].end(); it++)
    {
        if(!(*it)["template"] || !(*it)["name"])
        {
            qDebug() << "Invalid entity";
            continue;
        }

        QString templateName = QString::fromStdString((*it)["name"].as<std::string>());
        if(scene->templateList.count(templateName) == 0)
        {
            qDebug() << "Can't find template " << templateName;
            continue;
        }

        Entity *newEntity = new Entity(&scene->templateList[templateName]);
        scene->addItem(newEntity);

        for(YAML::iterator it2 = (*it)["components"].begin(); it2 != (*it)["components"].end(); it2++)
        {
            //Needs only info from CBox, CZOrder component
            if(!(*it2)["component"])
            {
                qDebug() << "Can't read a component";
                continue;
            }

            if((*it2)["component"].as<std::string>() == "CBox")
            {
                float x = (*it2)["x"] ? (*it2)["x"].as<float>() : 0.f;
                float y = (*it2)["y"] ? (*it2)["y"].as<float>() : 0.f;

                newEntity->setPos(x, y);
            }
            if((*it2)["component"].as<std::string>() == "CZOrder")
            {
                int zOrder = (*it2)["zOrder"] ? (*it2)["zOrder"].as<int>() : 0;
                int layer = (*it2)["layer"] ? (*it2)["layer"].as<int>() : 0;

                newEntity->SetLayer(layer);
                newEntity->SetZOrder(zOrder);
            }
        }
    }
}

void MainWindow::SaveLevel(QString fileName)
{
    YAML::Node node;

    node["levelBoundaries"]["x"] = scene->GetLevelBoundaries().x();
    node["levelBoundaries"]["x2"] = scene->GetLevelBoundaries().x() + scene->GetLevelBoundaries().width();
    node["levelBoundaries"]["y"] = scene->GetLevelBoundaries().y();
    node["levelBoundaries"]["y2"] = scene->GetLevelBoundaries().y() + scene->GetLevelBoundaries().height();

    for(int a = 0; a < scene->items().size(); a++)
    {
        YAML::Node entityNode;

        if(scene->items().at(a)->type() != QGraphicsItem::UserType + 1)
            continue;

        Entity *item = qgraphicsitem_cast<Entity*>(scene->items().at(a));
        if(!item)
            continue;

        entityNode["template"] = item->fileName.toStdString();
        entityNode["name"] = item->name.toStdString();

        //Save the box in CBox component
        YAML::Node cboxNode;
        cboxNode["component"] = "CBox";
        cboxNode["x"] = item->x();
        cboxNode["y"] = item->y();
        entityNode["components"].push_back(cboxNode);

        //Set the zOrder and the layer in CRender component
        YAML::Node czNode;
        czNode["component"] = "CZOrder";
        czNode["layer"] = item->GetLayer();
        czNode["zOrder"] = item->GetZOrder();
        entityNode["components"].push_back(czNode);

        //If the template has a CPhysic component, set the layer to the CPhysic component
        if(scene->templateList[item->name].listOfComponents.contains("CPhysic"))
        {
            YAML::Node cpNode;
            cpNode["component"] = "CPhysic";
            cpNode["layer"] = item->GetLayer();
            entityNode["components"].push_back(cpNode);
        }

        //If the template has a CObstacle component, set the layer to the CObstacle component
        if(scene->templateList[item->name].listOfComponents.contains("CObstacle"))
        {
            YAML::Node coNode;
            coNode["component"] = "CObstacle";
            coNode["layer"] = item->GetLayer();
            entityNode["components"].push_back(coNode);
        }

        node["entities"].push_back(entityNode);
    }

    std::filebuf fb;
    fb.open (fileName.toStdString().c_str(), std::ios::out);
    std::ostream os(&fb);

    YAML::Emitter emitter(os);
    emitter << node;

    fb.close();
}

void MainWindow::SetCurrentLayer(int currentLayer)
{
    scene->SetCurrentLayer(currentLayer);

    ui->actionBackground_Layer->setChecked(false);
    ui->actionLevel_Layer->setChecked(false);
    ui->actionForeground_Layer->setChecked(false);

    if(currentLayer == -1)
        ui->actionBackground_Layer->setChecked(true);
    else if(currentLayer == 0)
        ui->actionLevel_Layer->setChecked(true);
    else if(currentLayer == 1)
        ui->actionForeground_Layer->setChecked(true);
}

void MainWindow::OnModeChanged(QAction *modeAction)
{
    ui->propertiesDock->hide();
    ui->tilesCollectionDock->hide();
    ui->actionGridDuplication->setEnabled(false);
    ui->actionGridDuplicationMargin->setEnabled(false);

    if(modeAction == ui->actionInsertionMode)
    {
        //Go to insertion mode
        scene->SetEditionMode(EditorScene::InsertionMode);

        ui->tilesCollectionDock->show();
        ui->editorView->setDragMode(QGraphicsView::NoDrag);
    }
    else if(modeAction == ui->actionSelectionMode)
    {
        //Go to selection mode
        scene->SetEditionMode(EditorScene::SelectionMode);

        ui->propertiesDock->show();
        ui->editorView->setDragMode(QGraphicsView::RubberBandDrag);

        ui->actionGridDuplication->setEnabled(true);
        ui->actionGridDuplicationMargin->setEnabled(true);
    }
    else if(modeAction == ui->actionFillMode)
    {
        //Go to fill mode
        scene->SetEditionMode(EditorScene::FillMode);

        ui->tilesCollectionDock->show();
        ui->editorView->setDragMode(QGraphicsView::NoDrag);
    }
}

void MainWindow::on_actionOpen_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(this, "Open level", ".", "Level files (*.level)");

    if(fileName == "")
        return;

    LoadLevel(fileName);
    currentFileName = fileName;
}

void MainWindow::on_actionSaveAs_triggered()
{
    QString fileName = QFileDialog::getSaveFileName(this, "Save level", ".", "Level files (*.level)");

    if(fileName == "")
        return;

    SaveLevel(fileName);
    currentFileName = fileName;
}

void MainWindow::on_actionSave_triggered()
{
    if(currentFileName == "")
    {
        //hasn't been saved yet
        on_actionSaveAs_triggered();
    }
    else
    {
        SaveLevel(currentFileName);
    }
}

void MainWindow::on_actionNew_triggered()
{
    NewLevel();
}

void MainWindow::on_actionExit_triggered()
{
    qApp->quit();
}

void MainWindow::on_actionBackground_Layer_triggered()
{
    SetCurrentLayer(-1);
}

void MainWindow::on_actionLevel_Layer_triggered()
{
    SetCurrentLayer(0);
}

void MainWindow::on_actionForeground_Layer_triggered()
{
    SetCurrentLayer(1);
}

void MainWindow::on_actionShowLayerColor_triggered()
{
    scene->ShowLayerColor(ui->actionShowLayerColor->isChecked());
    scene->update();
}

void MainWindow::on_actionTestLevel_triggered()
{
    QMessageBox::StandardButton bt = QMessageBox::information(this, "Test the level", "The level has to be saved before testing it.\nDo you want to save it ?", QMessageBox::Yes|QMessageBox::Cancel, QMessageBox::Cancel);

    if(bt == QMessageBox::Cancel)
        return;

    on_actionSave_triggered();

    QStringList arguments;
    arguments << "--level=" + currentFileName + "" << "--players=1";

    QProcess::execute("Platformer Challenge.exe", arguments);
}

void MainWindow::UpdatePropertiesGrid()
{
    //Disable/Enable different widgets for properties
    if(scene->selectedItems().size() == 0)
    {
        ui->xSpin->setEnabled(false);
        ui->ySpin->setEnabled(false);
        ui->layerCombo->setEnabled(false);
        ui->zOrderSpin->setEnabled(false);

        return;
    }

    if(scene->selectedItems().size() > 1)
    {
        ui->xSpin->setEnabled(false);
        ui->ySpin->setEnabled(false);
    }
    else
    {
        ui->xSpin->setEnabled(true);
        ui->ySpin->setEnabled(true);
    }
    ui->layerCombo->setEnabled(true);
    ui->zOrderSpin->setEnabled(true);

    //Set the values of widgets
    Entity *firstSelected = qgraphicsitem_cast<Entity*>(scene->selectedItems()[0]);
    if(scene->selectedItems().size() > 1)
    {
        ui->layerCombo->setCurrentIndex(firstSelected->GetLayer() + 1);
        ui->zOrderSpin->setValue(firstSelected->GetZOrder());
    }
    else
    {
        ui->xSpin->setValue(firstSelected->x());
        ui->ySpin->setValue(firstSelected->y());
        ui->layerCombo->setCurrentIndex(firstSelected->GetLayer() + 1);
        ui->zOrderSpin->setValue(firstSelected->GetZOrder());
    }
}

void MainWindow::on_layerCombo_currentIndexChanged(int index)
{
    if(scene->selectedItems().size() > 0)
    {
        for(int a = 0; a < scene->selectedItems().size(); a++)
        {
            Entity *entity = qgraphicsitem_cast<Entity*>(scene->selectedItems()[a]);
            entity->SetLayer(index - 1);
        }
    }
    scene->update();
}

void MainWindow::on_zOrderSpin_editingFinished()
{
    if(scene->selectedItems().size() > 0)
    {
        for(int a = 0; a < scene->selectedItems().size(); a++)
        {
            Entity *entity = qgraphicsitem_cast<Entity*>(scene->selectedItems()[a]);
            entity->SetZOrder(ui->zOrderSpin->value());
        }
    }
    scene->update();
}

void MainWindow::on_xSpin_editingFinished()
{
    if(scene->selectedItems().size() == 1)
    {
        Entity *entity = qgraphicsitem_cast<Entity*>(scene->selectedItems()[0]);
        entity->setX(ui->xSpin->value());
    }
    scene->update();
}

void MainWindow::on_ySpin_editingFinished()
{
    if(scene->selectedItems().size() == 1)
    {
        Entity *entity = qgraphicsitem_cast<Entity*>(scene->selectedItems()[0]);
        entity->setY(ui->ySpin->value());
    }
    scene->update();
}

void MainWindow::on_actionGridDuplication_triggered()
{
    if(scene->selectedItems().size() != 1)
        return;

    int columns = QInputDialog::getInt(this, "Duplicate on a grid", "Number of column (X-axis)", 1, 1);
    int rows = QInputDialog::getInt(this, "Duplicate on a grid", "Number of lines (Y-axis)", 1, 1);

    Entity *selectedEntity = qgraphicsitem_cast<Entity*>(scene->selectedItems()[0]);
    float width(selectedEntity->boundingRect().width()), height(selectedEntity->boundingRect().height());

    for(int a = 0; a < columns; a++)
    {
        for(int b = 0; b < rows; b++)
        {
            if(a == 0 && b == 0)
                continue;

            Entity *newEntity = selectedEntity->Clone();
            newEntity->moveBy(a * width, b * height);
        }
    }
}

void MainWindow::on_actionGridDuplicationMargin_triggered()
{
    if(scene->selectedItems().size() != 1)
        return;

    int columns = QInputDialog::getInt(this, "Duplicate on a grid (with margins)", "Number of column (X-axis)", 1, 1);
    int rows = QInputDialog::getInt(this, "Duplicate on a grid (with margins)", "Number of lines (Y-axis)", 1, 1);

    float xMargin(0), yMargin(0);
    if(columns > 1)
        xMargin = QInputDialog::getDouble(this, "Duplication on a grid (with margins)", "Margin on X-axis", 0.d);
    if(rows > 1)
        yMargin = QInputDialog::getDouble(this, "Duplication on a grid (with margins)", "Margin on Y-axis", 0.d);

    Entity *selectedEntity = qgraphicsitem_cast<Entity*>(scene->selectedItems()[0]);
    float width(selectedEntity->boundingRect().width()), height(selectedEntity->boundingRect().height());

    for(int a = 0; a < columns; a++)
    {
        for(int b = 0; b < rows; b++)
        {
            if(a == 0 && b == 0)
                continue;

            Entity *newEntity = selectedEntity->Clone();
            newEntity->moveBy(a * width + std::max(0.f, xMargin * (a)), b * height + std::max(0.f, yMargin * (b)));
        }
    }
}

void MainWindow::UpdateLevelBoundaries(QRectF boundaries)
{
    ui->levelBoundXSpin->setValue(boundaries.x());
    ui->levelBoundX2Spin->setValue(boundaries.bottomRight().x());
    ui->levelBoundYSpin->setValue(boundaries.y());
    ui->levelBoundY2Spin->setValue(boundaries.bottomRight().y());
}

void MainWindow::on_levelBoundXSpin_valueChanged(int value)
{
    QRectF rect = scene->GetLevelBoundaries();
    rect.setX(ui->levelBoundXSpin->value());

    scene->SetLevelBoundaries(rect);
}

void MainWindow::on_levelBoundYSpin_valueChanged(int value)
{
    QRectF rect = scene->GetLevelBoundaries();
    rect.setY(ui->levelBoundYSpin->value());

    scene->SetLevelBoundaries(rect);
}

void MainWindow::on_levelBoundX2Spin_valueChanged(int value)
{
    QRectF rect = scene->GetLevelBoundaries();
    rect.setWidth(ui->levelBoundX2Spin->value() - ui->levelBoundXSpin->value());

    scene->SetLevelBoundaries(rect);
}

void MainWindow::on_levelBoundY2Spin_valueChanged(int value)
{
    QRectF rect = scene->GetLevelBoundaries();
    rect.setHeight(ui->levelBoundY2Spin->value() - ui->levelBoundYSpin->value());

    scene->SetLevelBoundaries(rect);
}

void MainWindow::on_listWidget_itemClicked(QTreeWidgetItem *item, int column)
{
    if(scene->GetEditionMode() == EditorScene::InsertionMode || scene->GetEditionMode() == EditorScene::FillMode)
        scene->SetItemToBeInserted(item->data(0, Qt::UserRole).toString());
}
