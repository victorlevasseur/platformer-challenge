#include "editorscene.h"

#include <QMimeData>
#include <QDebug>
#include <QListWidgetItem>
#include <QDataStream>
#include <QByteArray>

EditorScene::EditorScene(QObject *parent) :
    QGraphicsScene(parent),
    editionMode(SelectionMode),
    tileToInsert(""),
    tileToInsertItem(0),
    gridSize(70.f, 70.f),
    currentLayer(0),
    levelBoundaries(0, 0, 1024, 768),
    boundariesRectItem(0)
{
    setBackgroundBrush(QBrush(QColor(120, 120, 120)));

    SetCurrentLayer(0);
    UpdateBoundaries();
}

void EditorScene::SetEditionMode(EditionMode mode)
{
    editionMode = mode;
    clearSelection();
    SetItemToBeInserted("");
}

EditorScene::EditionMode EditorScene::GetEditionMode() const
{
    return editionMode;
}

void EditorScene::SetCurrentLayer(int layer)
{
    currentLayer = layer;
}

void EditorScene::ShowLayerColor(bool show)
{
    showLayerColor = show;

}

void EditorScene::drawForeground(QPainter * painter, const QRectF & rect)
{
    //Draw the selection rectangle (blue) on selected items
    for(int a = 0; a < selectedItems().size(); a++)
    {
        painter->setPen(QPen(QColor(128, 128, 255, 180)));
        painter->setBrush(QBrush(QColor(128, 128, 255, 128)));
        painter->drawRect(QRect(selectedItems()[a]->x(),
                                selectedItems()[a]->y(),
                                selectedItems()[a]->boundingRect().width(),
                                selectedItems()[a]->boundingRect().height()));
    }
}

void EditorScene::keyReleaseEvent(QKeyEvent * keyEvent)
{
    //Delete the selected items when Delete key is pressed
    if(keyEvent->key() == Qt::Key_Delete)
    {
        while(selectedItems().size() > 0)
            removeItem(selectedItems().at(0));
    }

    QGraphicsScene::keyReleaseEvent(keyEvent);
}

void EditorScene::mousePressEvent(QGraphicsSceneMouseEvent * mouseEvent)
{
    if(editionMode == SelectionMode)
    {
        QGraphicsScene::mousePressEvent(mouseEvent);
    }

    if(tileToInsert != "" && tileToInsertItem != 0)
    {
        if(editionMode == InsertionMode)
        {
            Entity *entity = new Entity(&templateList[tileToInsert]);
            addItem(entity);
            lastInsertedTilePos = GetPositionOnGrid(mouseEvent->scenePos());
            entity->setPos(lastInsertedTilePos);
            entity->SetLayer(currentLayer);
        }
        else if(editionMode == FillMode)
        {
            Fill(GetPositionOnGrid(mouseEvent->scenePos()));
        }
    }
}

void EditorScene::mouseMoveEvent(QGraphicsSceneMouseEvent * mouseEvent)
{
    if(editionMode == SelectionMode)
    {
        QGraphicsScene::mouseMoveEvent(mouseEvent);
    }

    //Move the tile to be inserted
    if(tileToInsertItem)
    {
        tileToInsertItem->setPos(GetPositionOnGrid(mouseEvent->scenePos()));

        if(editionMode == InsertionMode)
        {
            //Add another entity if the mouse button is pressed and if the entity is next the previous one
            if((mouseEvent->buttons() & Qt::LeftButton) != 0)
            {
                QPointF nextPos(GetPositionOnGrid(mouseEvent->scenePos()));
                if(abs(nextPos.x() - lastInsertedTilePos.x()) >= tileToInsertItem->boundingRect().width() ||
                   abs(nextPos.y() - lastInsertedTilePos.y()) >= tileToInsertItem->boundingRect().height())
                {
                    //Add the new item
                    Entity *entity = new Entity(&templateList[tileToInsert]);
                    addItem(entity);
                    entity->setPos(nextPos);
                    entity->SetLayer(currentLayer);

                    //If is colliding with a entity of the same type, delete it
                    QList<QGraphicsItem*> collidingItems = entity->collidingItems(Qt::IntersectsItemBoundingRect);
                    for(int a = 0; a < collidingItems.size(); a++)
                    {
                        if(collidingItems[a]->type() == QGraphicsItem::UserType + 1)
                        {
                            if(qgraphicsitem_cast<Entity*>(collidingItems[a])->name == entity->name)
                            {
                                removeItem(entity);
                            }
                        }
                    }

                    lastInsertedTilePos = nextPos;
                }
            }
        }
    }
}

void EditorScene::mouseReleaseEvent(QGraphicsSceneMouseEvent * mouseEvent)
{
    if(editionMode == SelectionMode)
    {
        QGraphicsScene::mouseReleaseEvent(mouseEvent);
    }
}

void EditorScene::ShowLayer(int layer)
{
    for(unsigned int a = 0; a < items().size(); a++)
    {
        if(items().at(a)->type() != QGraphicsItem::UserType + 1)
            continue;

        Entity *item = qgraphicsitem_cast<Entity*>(items().at(a));

        if(item->GetLayer() == layer)
        {
            item->show();
        }
    }
}

void EditorScene::HideLayer(int layer)
{
    for(unsigned int a = 0; a < items().size(); a++)
    {
        if(items().at(a)->type() != QGraphicsItem::UserType + 1)
            continue;

        Entity *item = qgraphicsitem_cast<Entity*>(items().at(a));

        if(item->GetLayer() == layer)
        {
            item->hide();
        }
    }
}

void EditorScene::UpdateBoundaries()
{
    //Update the bounding rect indicator
    if(!boundariesRectItem)
    {
        boundariesRectItem = addRect(levelBoundaries, QPen(Qt::NoPen), QBrush(QColor(145, 210, 255)));
        boundariesRectItem->setZValue(-1000000000);
    }
    else
    {
        boundariesRectItem->setRect(levelBoundaries);
    }

    //Update the scrollbars maximums an minimums
    setSceneRect(levelBoundaries.x() - 1024, levelBoundaries.y() - 768,
                 levelBoundaries.width() + 2*1024, levelBoundaries.height() + 2*768);

    emit LevelBoundariesUpdated(levelBoundaries);
}

void EditorScene::SetItemToBeInserted(QString itemName)
{
    //Update the tile preview
    tileToInsert = itemName;
    if(tileToInsert != "")
    {
        if(!tileToInsertItem)
        {
            tileToInsertItem = addPixmap(QPixmap(templateList[tileToInsert].texturePath));
            tileToInsertItem->setZValue(100000000);
        }
        else if(tileToInsertItem)
        {
            tileToInsertItem->setPixmap(QPixmap(templateList[tileToInsert].texturePath));
        }
    }
    else
    {
        removeItem(tileToInsertItem);
        tileToInsertItem = 0;
    }
}

QPointF EditorScene::GetPositionOnGrid(QPointF position)
{
    QPointF newPos;

    newPos.setX((int)((position.x()) / gridSize.x()) * gridSize.x() - (position.x() < 0 ? gridSize.x() : 0));
    newPos.setY((int)((position.y()) / gridSize.y()) * gridSize.y() - (position.y() < 0 ? gridSize.y() : 0));

    return newPos;
}

void EditorScene::Fill(QPointF firstItemPos)
{
    if(tileToInsert == "" || !tileToInsertItem)
        return;

    Entity *entity = new Entity(&templateList[tileToInsert]);
    addItem(entity);
    entity->setPos(firstItemPos);
    entity->SetLayer(currentLayer);

    fillIteration = 0;
    FillAdjacentTiles(entity);
}

void EditorScene::FillAdjacentTiles(Entity *entity)
{
    if(fillIteration > 1000)
        return;

    for(int a = 0; a < 4; a++)
    {
        Entity *newEntity = entity->Clone();
        switch (a)
        {
        case 0:
            newEntity->moveBy(entity->boundingRect().width(), 0.f);
            break;
        case 1:
            newEntity->moveBy(0.f, entity->boundingRect().height());
            break;
        case 2:
            newEntity->moveBy(-entity->boundingRect().width(), 0.f);
            break;
        case 3:
            newEntity->moveBy(0.f, -entity->boundingRect().height());
            break;
        }

        bool colliding = false;
        for(unsigned int a = 0; a < newEntity->collidingItems(Qt::IntersectsItemBoundingRect).size(); a++)
        {
            if(newEntity->collidingItems(Qt::IntersectsItemBoundingRect)[a]->type() == QGraphicsItem::UserType + 1 &&
               newEntity->collidingItems(Qt::IntersectsItemBoundingRect)[a] != newEntity)
            {
                colliding = true;
                break;
            }
        }

        bool outOfBoundings = false;
        if(newEntity->x() + newEntity->boundingRect().width() <= levelBoundaries.x() ||
           newEntity->x() >= levelBoundaries.x() + levelBoundaries.width() ||
           newEntity->y() + newEntity->boundingRect().height() <= levelBoundaries.y() ||
           newEntity->y() >= levelBoundaries.y() + levelBoundaries.height())
        {
            outOfBoundings = true;
        }

        if(colliding || outOfBoundings)
            removeItem(newEntity);
        else
        {
            fillIteration++;
            FillAdjacentTiles(newEntity);
        }
    }
}
