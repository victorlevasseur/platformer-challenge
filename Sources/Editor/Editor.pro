#-------------------------------------------------
#
# Project created by QtCreator 2014-02-02T13:29:11
#
#-------------------------------------------------

QT += widgets

TARGET = Editor
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
    editorscene.cpp \
    entity.cpp \
    template.cpp

HEADERS  += mainwindow.h \
    editorscene.h \
    entity.h \
    template.h

Release:DESTDIR = release
Release:OBJECTS_DIR = release/.editor/obj
Release:MOC_DIR = release/.editor/moc
Release:RCC_DIR = release/.editor/rcc
Release:UI_DIR = release/.editor/ui

Debug:DESTDIR = debug
Debug:OBJECTS_DIR = debug/.editor/obj
Debug:MOC_DIR = debug/.editor/moc
Debug:RCC_DIR = debug/.editor/rcc
Debug:UI_DIR = debug/.editor/ui

MAKEFILE=EditorMakefile

FORMS    += mainwindow.ui

INCLUDEPATH += $$PWD/../../Libraries/boost_1_55_0

win32: LIBS += -L$$PWD/../../Libraries/yaml-cpp/bin/ -lyaml-cpp

INCLUDEPATH += $$PWD/../../Libraries/yaml-cpp/include
DEPENDPATH += $$PWD/../../Libraries/yaml-cpp/include

PRE_TARGETDEPS += $$PWD/../../Libraries/yaml-cpp/bin/libyaml-cpp.a

RESOURCES += \
    resources.qrc
