#ifndef TEMPLATE_H
#define TEMPLATE_H

#include <QString>
#include <QList>
#include "yaml-cpp/yaml.h"

class Template
{
public:
    Template();
    Template(YAML::Node &node, QString _entityFileName);

    QString name;
    QString entityFileName;

    QString fullName;
    QString category;
    QString iconPath;
    QString texturePath;

    QList<QString> listOfComponents;
};

#endif // TEMPLATE_H
