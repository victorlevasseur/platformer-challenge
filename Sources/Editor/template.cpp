#include "template.h"

Template::Template()
{

}

Template::Template(YAML::Node &node, QString _entityFileName)
{
    entityFileName = _entityFileName;
    if(node["name"])
        name = QString::fromStdString(node["name"].as<std::string>());

    if(node["editor"])
    {
        if(node["editor"]["name"])
        {
            fullName = QString::fromStdString(node["editor"]["name"].as<std::string>());
        }
        if(node["editor"]["icon"])
        {
            iconPath = QString::fromStdString(node["editor"]["icon"].as<std::string>());
        }
        if(node["editor"]["name"])
        {
            texturePath = QString::fromStdString(node["editor"]["texture"].as<std::string>());
        }
        if(node["editor"]["category"])
        {
            category = QString::fromStdString(node["editor"]["category"].as<std::string>());
        }
    }

    if(node["components"])
    {
        for(YAML::iterator it = node["components"].begin(); it != node["components"].end(); it++)
        {
            if((*it)["component"])
            {
                listOfComponents.append(QString::fromStdString((*it)["component"].as<std::string>()));
            }
        }
    }
}
