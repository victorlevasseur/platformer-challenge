#include "maindialog.h"
#include "ui_maindialog.h"

#include <QFileDialog>
#include <QProcess>
#include <QMessageBox>

MainDialog::MainDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MainDialog)
{
    ui->setupUi(this);

    ui->bannerLabel->setPixmap(QPixmap("res/Launcher/LauncherBanner.png"));
    ui->playLevelBt->setIcon(QIcon("res/HUD/hud_p1.png"));

    on_playLevelBt_toggled(false);

    InsertPlayerList(ui->player1Combo);
    InsertPlayerList(ui->player2Combo, true);
    InsertPlayerList(ui->player3Combo, true);
    InsertPlayerList(ui->player4Combo, true);
}

MainDialog::~MainDialog()
{
    delete ui;
}

void MainDialog::on_playLevelBt_toggled(bool checked)
{
    ui->playLevelPanel->setVisible(checked);
    ui->playLevelBt->setChecked(checked);
}

void MainDialog::on_browseBt_clicked()
{
    QString levelFileName = QFileDialog::getOpenFileName(this,
                                                         "Level to play",
                                                         QDir::currentPath() + "/levels",
                                                         "Levels (*.level);;All (*)");
    ui->levelPathEdit->setText(levelFileName);
}

void MainDialog::InsertPlayerList(QComboBox *combo, bool optionalPlayer)
{
    //Insert "No player"
    if(optionalPlayer)
    {
        combo->addItem(QIcon("res/HUD/hud_x.png"), "No player");
    }

    //Insert players
    for(int a = 0; a < 4; a++)
    {
        combo->addItem(QIcon("res/HUD/hud_p" + QString::number(a + 1) + "Alt.png"), "Alien #" + QString::number(a + 1));
    }
}

QString MainDialog::GenerateCommandLine()
{
    QString commandLine("\"Platformer Challenge.exe\" ");

    //Level
    commandLine += "--level=\"" + ui->levelPathEdit->text() + "\" ";

    //Players
    commandLine += "--players=";

    commandLine += QString::number(ui->player1Combo->currentIndex() + 1);
    if(ui->player2Combo->currentIndex() != 0)
    {
        commandLine += "," + QString::number(ui->player2Combo->currentIndex());
    }
    if(ui->player3Combo->currentIndex() != 0)
    {
        commandLine += "," + QString::number(ui->player3Combo->currentIndex());
    }
    if(ui->player4Combo->currentIndex() != 0)
    {
        commandLine += "," + QString::number(ui->player4Combo->currentIndex());
    }

    return commandLine;
}

void MainDialog::on_toolButton_clicked()
{
    QProcess::execute(GenerateCommandLine());
}

void MainDialog::on_quitBt_clicked()
{
    qApp->quit();
}

void MainDialog::on_editorBt_clicked()
{
    on_playLevelBt_toggled(false);
    QProcess::execute("Editor.exe");
}
