#-------------------------------------------------
#
# Project created by QtCreator 2014-02-12T15:21:28
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Launcher
TEMPLATE = app

Release:DESTDIR = release
Release:OBJECTS_DIR = release/.launcher/obj
Release:MOC_DIR = release/.launcher/moc
Release:RCC_DIR = release/.launcher/rcc
Release:UI_DIR = release/.launcher/ui

Debug:DESTDIR = debug
Debug:OBJECTS_DIR = debug/.launcher/obj
Debug:MOC_DIR = debug/.launcher/moc
Debug:RCC_DIR = debug/.launcher/rcc
Debug:UI_DIR = debug/.launcher/ui

MAKEFILE=LauncherMakefile

SOURCES += main.cpp\
        maindialog.cpp

HEADERS  += maindialog.h

FORMS    += maindialog.ui
