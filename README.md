# Platformer Challenge
##Presentation
Platformer Challenge is a platformer game with a big customization.Create your own levels, your own world with a lot of predefined tiles and object. You can even create your own blocks and share them. 

##Compiling the game and the level editor
The game depends on SFML, entityx, Thor, yaml-cpp and Boost and the Level editor uses Qt and yaml-cpp.
Compile the game and the level editor, then put the executables and their dependencies in a single folder. Finally, copy the res, level and templates folders alongside the game. It should work.

##Customization
All blocks can be edited. Just create an .entity file in the template folder and take the provided block as an example. You can put several block in a single file.

##Status and licence
For the moment, the project is at his beginning. The game only open a single level : levels/TestLevel.level
The game and the editor are distributed under the GNU GPLv3.